﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance, Ignore
#Include, include

;Check for admin rights (required for netsh commands)
if not A_IsAdmin
{
   Run *RunAs "%A_ScriptFullPath%"  ; Requires v1.0.92.01+
   ExitApp
}

If A_IsCompiled && (A_ScriptDir != A_ProgramFiles . "\iFace Switch")
{
	FileCreateDir, % A_ProgramFiles . "\iFace Switch"
	FileInstall, sqlite3.dll, % A_ProgramFiles . "\iFace Switch\sqlite3.dll"
	FileMove, % A_ScriptFullPath, % A_ProgramFiles . "\iFace Switch\" . A_ScriptName, 1
	Run, % A_ProgramFiles . "\iFace Switch\" . A_ScriptName
	ExitApp
}

db := "settings.db"
varList := "IPAddress|Subnet|Gateway|DNS1|DNS2"

;check for the iniFile and create itif it doesn'texist
If !FileExist(db)
{
	iFace_createDB()
}

;Build the UI
Gui, +OwnDialogs
Gui, Add, Button, w40 Section, New
Gui, Add, Button, w40 ys x+5, Save
Gui, Add, DDL, w170 ys+1 x+5 vconfig gchangeConfig, % iFace_getConfigs(1) 
GuiControl, ChooseString, config, % iFace_getChosenConfig(1)

Gui, Add, Text, w100 xs y+15 Section Center, Interface
Gui, Add, DDL, w150 ys-3 vInterface, % iFace_getIFaces()

Gui, Add, Text, w100 xs y+15 Section Center, IP Address
Gui, Add, Edit, w150 ys-3 vIPAddress,
Gui, Add, Text, w100 xs Section Center, Subnet Mask
Gui, Add, Edit, w150 ys-3 vSubnet,
Gui, Add, Text, w100 xs Section Center, Gateway
Gui, Add, Edit, w150 ys-3 vGateway,

Gui, Add, Text, w100 xs y+15 Section Center, DNS 1
Gui, Add, Edit, w150 ys-3 vDNS1,
Gui, Add, Text, w100 xs Section Center, DNS 2
Gui, Add, Edit, w150 ys-3 vDNS2,

Gui, Add, Button, w125 xs y+15 Section, Set DHCP
Gui, Add, Button, w125 ys, Set IP

GoSub, changeConfig

Gui, Show, AutoSize, iFace Switch
Return

;When the Gui is closed or the application exits gracefully
OnExit:
GuiClose:
ExitApp
Return

;A new config needs to be created.weaddthe necessary entries to the database
ButtonNew:
InputBox, NewConfig, New config name?, Name your new config
If ErrorLevel
{
	Return
}
iFace_newConfig(NewConfig)
GuiControl, , config, % "|" . iFace_getConfigs(1)
GuiControl, ChooseString, config, % "|" . iFace_getChosenConfig(1)
Return

;Save a config.save the GUI values to the database
ButtonSave:
Gui, Submit, NoHide
SQL_Exec("UPDATE settings SET value='" . Interface . "' WHERE name='Interface' AND config='" . config . "';")
Loop, Parse, varList,|
{
	If !A_Loopfield
	{
		Continue
	}
	SQL_Exec("UPDATE settings SET value='" . %A_LoopField% . "' WHERE name='" . A_LoopField . "' AND config='" . config . "';")    
}
Return

;This runs when a new config is chosen in the dropdown menu.
;Weset the chosen config as the Chosen config in the database,clearthe UI and fill inwith the entires for the chosen config
changeConfig:
Gui, Submit, NoHide
iFace_setChosenConfig(config)
GuiControl, ChooseString, Interface, % ""
Loop, Parse, varList, |
{
	If !A_Loopfield
	{
		Continue
	}
	GuiControl, , % A_LoopField, 
}
iFace := SQL_Get("SELECT value FROM settings WHERE name='Interface' AND config='" . config . "';")
iFaces := iFace_getIFaces()
Loop, Parse, iFaces, |
{
	if (A_LoopField = iFace)
	{
		iFaceNum := A_Index
		Break
	}
}
GuiControl, Choose, Interface, % iFaceNum  
Loop, Parse, varList, |
{
	If !A_Loopfield
	{
		Continue
	}
	GuiControl, , % A_Loopfield, % SQL_Get("SELECT value FROM settings WHERE name='" . A_LoopField . "' AND config='" . config . "';")
}
Return
 
;Use netsh to set the chosen interface to DHCP
ButtonSetDHCP:
GoSub, ButtonSave	
iFace_netshDHCP(config)
Return 
 
;Use netsh to set the chosen interface to the specified IP address
ButtonSetIP: 
GoSub, ButtonSave
iFace_netshIP(config)
Return
