SQL_Get(statement, return=true) {
	Global db
	Global names = 
	Global data = 
	ErrorLevel = 0
	recordSet =
	row =
	sql := new SQLiteDB
	If !sql.OpenDB(db)
	{
		ErrorLevel = 1
		Return
	}
	StringRight, isSemiColon, statement, 1
	If (isSemiColon == ";")
	{
		StringTrimRight, statement, statement, 1
	}
	If !sql.Query(statement, recordSet)
	{
		If !recordSet
		{
			ErrorLevel = 1
			Return
		}
	}
	If (recordSet.HasNames) 
	{
		Loop, % recordSet.ColumnCount
		{
			names .= recordSet.ColumnNames[A_Index] . "|"
		}
		StringTrimRight, names, names, 1
	}
	names = %names%
	Loop
	{
		next := recordSet.Next(row)
		If (next < 1)
		{
			Break
		}
		Loop, % recordSet.ColumnCount ;%
		{
			data .= row[A_Index] . "|"
		}
		StringTrimRight, data, data, 1
		data .= "`n"
	}
	StringTrimRight, data, data, 1
	data = %data%
	If return
	{
		Return data
	}
	Else
	{
		Return data
	}
	sql.CloseDB()
}

SQL_Exec(statement) {
	Global db
	ErrorLevel = 0
	sql := new SQLiteDB
	If !sql.OpenDB(db)
	{
		ErrorLevel = 1
		Return
	}
	StringRight, isSemiColon, statement, 1
	If (isSemiColon == ";")
	{
		StringTrimRight, statement, statement, 1
	}
	if !sql.Exec(statement)
	{
		ErrorLevel = 1
		Return
	}
	sql.SetTimeout(1000)
	sql.CloseDB()
}