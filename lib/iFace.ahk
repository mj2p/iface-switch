﻿;Error Catching
iFace_error(error=0) {
	Msgbox, 16, iFace Switch, Ooops!`nSomething went wrong.`n`n%error%
	Return
}

;Logging
iFace_log(text) {
	FileAppend, %A_Now% - %text%, iFace.log
	Return
}

;Set up the database file and provide the Default config
iFace_createDB() {
	global db
	SQL_Exec("CREATE TABLE configs(name);")
	SQL_Exec("CREATE TABLE settings(name,value,config);")
	SQL_Exec("INSERT INTO configs VALUES ('Default');")
	SQL_Exec("INSERT INTO settings VALUES ('chosenConfig', '', 'Default');")
	iFace_setParams("Default")
	
}

;For the given config, set up blank enteries so that updates can be used in future
iFace_setParams(config) {
	global db
	global varList
	SQL_Exec("INSERT INTO settings VALUES ('Interface','','" . config . "');")
	Loop, Parse, varList, |
	{
		SQL_Exec("INSERT INTO settings VALUES ('" . A_LoopField . "','','" . config . "');")
	}
	Return
}

; Scan the settings ini file for sections.
; Each section corresponds to a config
iFace_getConfigs(set=0) {
	global db
	static _configs
	If set
	{
		_configs := SQL_Get("SELECT name FROM configs;")
		StringReplace, _configs, _configs, `n, |, A
	}
	Return _configs
}

;Get the chosenConfig Variable from the database
iFace_getChosenConfig(set=0) {
	global db
	static _chosenConfig
	If set
	{
		_chosenConfig := SQL_Get("SELECT config FROM settings WHERE name='chosenConfig';")
	}
	Return _chosenConfig
}

;Set the given configas the Chosen config
iFace_setChosenConfig(config) {
	global db
	SQL_Exec("UPDATE settings SET config='" . config . "' WHERE name='chosenConfig';")
	Return
}

;Set up the given config as a new config ensure it is not a duplicate name
iFace_newConfig(config) {
	global db
	_checkConfigs := iFace_getConfigs()
	Loop, Parse, _checkConfigs, |
	{
		If (A_LoopField = config)
		{
			MsgBox, 16, iFace Switch, A config with that name already exists.`nPlease try again
			Return
		}
	}
	SQL_Exec("INSERT INTO configs VALUES ('" . config . "');")
	iFace_setParams(config)
	iFace_setChosenConfig(config)
	Return
}

;use comspec to run the given command
iFace_comspec(command) {
	RunWait, %comspec% /c %command% > comspec.txt, , hide
	FileRead, _comspec, comspec.txt
	FileDelete, comspec.txt
	Return _comspec
}

;use netsh to list the available network interfaces
iFace_getIFaces() {
	_interfaces := iFace_comspec("netsh interface ip show config")
	_iFaces := "|"
	Loop, Parse, _interfaces, `n
	{
		If InStr(A_LoopField, "Configuration for interface")
		{
			firstQuote := InStr(A_LoopField, """")
			_iFace := SubStr(A_LoopField, (firstQuote+1))
			If InStr(_iFace, "pseudo") || Instr(_iFace, "*")
			{
				Continue
			}
			_iFaces .= SubStr(_iFace, 1, (StrLen(_iFace)-2)) . "|"
		}
	}
	_iFaces := SubStr(_iFaces, 1, (StrLen(_iFaces)-1)) . "|"
	Sort, _iFaces, D|
	Return _iFaces
}	

;use netsh to set the interface to DHCP
iFace_netshDHCP(config) {
	iFace_showProgress(1)
	_interface := SQL_Get("SELECT value FROM settings WHERE name='Interface' AND config='" . config . "';")
	If !_interface
	{
		iFace_showProgress()
		iFace_error("No Interface specified")
		Return
	}
	iFace_log("Set " . _interface . " to DHCP - " . iFace_comspec("netsh interface ip set address """ . _interface . """ dhcp"))
	iFace_log("Set " . _interface . " DNS to DHCP - " . iFace_comspec("netsh interface ip set dns """ . _interface . """ dhcp"))
	iFace_showProgress()
	Msgbox, 0, iFace Switch, %_interface% set to DHCP, 5
	Return 
}

;use netsh toset the interface to IP
iFace_netshIP(config) {
	iFace_showProgress(1)
	_interface := SQL_Get("SELECT value FROM settings WHERE name='Interface' AND config='" . config . "';")
	_ipaddress := SQL_Get("SELECT value FROM settings WHERE name='IPAddress' AND config='" . config . "';") 
	_subnet := SQL_Get("SELECT value FROM settings WHERE name='Subnet' AND config='" . config . "';")
	_gateway := SQL_Get("SELECT value FROM settings WHERE name='Gateway' AND config='" . config . "';")
	_dns1 := SQL_Get("SELECT value FROM settings WHERE name='DNS1' AND config='" . config . "';")
	_dns2 :=  SQL_Get("SELECT value FROM settings WHERE name='DNS2' AND config='" . config . "';")
	
	If !_interface
	{
		iFace_showProgress()
		iFace_error("No Interface specified")
		Return
	}
	If !_ipaddress || !_subnet || !_gateway
	{
		iFace_showProgress()
		iFace_error("One or more details are missing")
		Return
	}
	iFace_log("Set " . _interface . " to " . _ipaddress . "|" . _subnet . "|" . _gateway . " - " . iFace_comspec("netsh interface ip set address """ . _interface . """ static " . _ipaddress . " " . _subnet . " " . _gateway))
	If _dns1
	{
		iFace_log("Set " . _interface . " dns1 to " . _dns1 . " - " . iFace_comspec("netsh interface ip set dns """ . _interface . """ static " . _dns1))
	}
	If _dns2
	{
		iFace_log("Set " . _interface . " dns2 to " . _dns2 . " - " . iFace_comspec("netsh interface ip add dns """ . _interface . """ " . _dns2 . " index=2"))
	}
	iFace_showProgress()
	Msgbox, 0, iFace Switch, Config %config% applied to %_interface%, 5
	Return
}

;Show a progress bar.
iFace_showProgress(start=0) {
	global progress
	If start
	{
		Gui, 2: -Caption +Border +Owner
		Gui, 2: Font, s11
		Gui, 2: Add, Text, w300 Center x15 y5 BackgroundTrans, Working
		Gui, 2: Add, Progress, vprogress w300 h10 x15 y35 BackgroundTrans c0080C0 range0-100
		Gui, 2: Show, AutoSize
		SetTimer, moveProg, 20
		Return
	}
	Else
	{
		SetTimer, moveProg, Off
		Gui, 2: Destroy
		Return
	}
	
	moveProg:
	prog += 1
	If (prog > 100)
	{
		prog = 0 
	}
	GuiControl, 2: , progress, %prog%
	Return
}