# iFace Switch

iFace Switch is a simple utility that can quickly and easily change the network settings of your network adapter.
I regularly work on two different networks. One needs a static IP address and the other is controlled by DHCP.
I got bored of having to use the dumb Windows IP configuration properties window to change my network settings.

iFace Switch is a Graphical Front end to the Netsh program which comes bundled with Windows.
You set the network details you want to use for your static IP network and can then switch easily between the static and DHCP networks by clicking a button.

You can also set up different network configurations which are stored by iFace Switch so that you can easily switch between them.

Currently iFace Switch only alters the IPv4 network settings.


iFace Switch is written in AutoHotkey
http://autohotkey.com

Changes:
-iFace Switch now uses a very simple sqlite database instead of ini files for storing the config data
-automitically scan the available network interfaces and display as a drop down to avoid mis typing 
-multiple configs can be stored
-simple progress bar when setting configs as error checking can mean a bit of a wait. (still less time than manual entry of ip details though)
